# Programming a 3D scanner in python
This repository consists of the code to estimate the 3D points of an object from structured light images.
As a start a simulation is also performed for testing the code.
The scanner is largely inspired by a course at [brown university](http://mesh.brown.edu/byo3d/)

Non python dependencies for the code is:
- Blender for simulation
# - OpenCV with python binders for the scanning
