import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="3Dscanner",
    version="0.0.1",
    author="Adrian Roth",
    author_email="roth.adrian@protonmail.com",
    description="Software for 2 different 3D scanning techniques, swept planes and stereo disparity",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/roth.adrian/3dscanner",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU GPLv3",
        "Operating System :: OS Independent",
    ],
    entry_points = {
            "console_scripts": [
                '3dscanner = threedscanner.__main__:main',
            ]
    },
    install_requires = ['numpy', 'scipy', 'scikit-image', 'moviepy', 'matplotlib', 'trimesh'],
    python_requires='>=3.6',
    # Including stuff for simulation
    package_data={
        "threedscanner": ["simulation/*.blend"],
    }
)
