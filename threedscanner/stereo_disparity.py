#!/usr/bin/python3
import cv2
import os.path as osp
import numpy as np
from skimage import io
import matplotlib.pyplot as plt

from . import helpers


# disparity range is tuned for simulated image pair
_block_size = 3
_min_disp = 0 # 90
_num_disp = 16*10
_disparity_matcher = cv2.StereoSGBM_create(
    minDisparity = _min_disp,
    numDisparities = _num_disp,
    blockSize = _block_size,
    P1 = 8*3*_block_size**2,
    P2 = 32*3*_block_size**2,
    disp12MaxDiff = 50,
    uniquenessRatio = 15,
    speckleWindowSize = 100,
    speckleRange = 1
)
# _disparity_matcher = cv2.StereoSGBM_create(0, 16*14)

def read_camera_calibrations(directory, config):
    camera_calibrations = {}
    append_str_to_dict_keys = lambda d, s: { '{}{}'.format(key, s): value for key, value in d.items() }
    camera1_calibration_base = helpers.read_json(osp.join(directory, config['camera1']))
    camera_calibrations.update(append_str_to_dict_keys(camera1_calibration_base, '1'))
    camera2_calibration_base = helpers.read_json(osp.join(directory, config['camera2']))
    camera_calibrations.update(append_str_to_dict_keys(camera2_calibration_base, '2'))
    return camera_calibrations

def find_rectification(camera1_image, camera2_image, camera_calibrations, config, verbose=False):
    """
    Input is stereo image captured on the scene with two orthogonal planes and four calibration points, see helpers.find_scene.
    The scene is found as key in config.
    Using this the maps to use with remap are estimated to get a rectified version of the images captured in the stereo setup. In other words calculate how to transform each camera to virtually get both images in the same plane.
    """
    shape = camera1_image.shape[:2][::-1]

    camera1_calibrations = { key[:-1]: value for key, value in camera_calibrations.items() if '1' in key }
    projection_matrix1 = helpers.extrinsic_camera_calibration(camera1_image, camera1_calibrations, config['scene']) 
    camera2_calibrations = { key[:-1]: value for key, value in camera_calibrations.items() if '2' in key }
    projection_matrix2 = helpers.extrinsic_camera_calibration(camera2_image, camera2_calibrations, config['scene']) 

    if 'real_projection_matrix1' in config and verbose:
        # Compare real to estimated projection matrices
        helpers.rodrigues(config['real_projection_matrix1']['rvec'])
        helpers.rodrigues(projection_matrix1['rvec'])
        print('projection_matrix comparison')
        print('Correct matrix1:')
        print(config['real_projection_matrix1'])
        print('Estimated:')
        print(projection_matrix1)
        print('Correct matrix2:')
        print(config['real_projection_matrix2'])
        print('Estimated:')
        print(projection_matrix2)
    if 'real_projection_matrix1' in config and 'real_projection_matrix2' in config:
        # Setting real matrix to be used
        projection_matrix1 = config['real_projection_matrix1']
        projection_matrix2 = config['real_projection_matrix2']

    # Estimating rotation and translation between the cameras
    # image_points1 = helpers.find_scene(camera1_image)
    # image_points2 = helpers.find_scene(camera2_image)
    # object_points = helpers.calculate_scene_points(config['scene'])
    # R, T = cv2.stereoCalibrate([object_points], [image_points1], [image_points2],
    #         **camera_calibrations, imageSize=shape)[5:7]


    # Calculate how to transform each camera to virtually get both images in the same plane
    # R1 and R2 are rectification transforms for respective images, P is the final camera matrix for rectified cameras and Q is the image disparity transform to 3D coordinates if the first camera center is coordinate origin
    R1, R2, P1, P2, Q = helpers.stereo_rectify(camera_calibrations, projection_matrix1, projection_matrix2)
    # print(R2)

    # R1_o = helpers.rodrigues(projection_matrix1['rvec'].copy())
    # R2_o = helpers.rodrigues(projection_matrix2['rvec'].copy())
    # t1_o = projection_matrix1['tvec'].copy()
    # t2_o = projection_matrix2['tvec'].copy()
    # R = R2_o @ R1_o.T
    # C1 = -R1_o.T @ t1_o
    # C2 = -R2_o.T @ t2_o
    # T = R1_o @ (C2 - C1)
    # R12, R22, P12, P22, Q = cv2.stereoRectify(**camera_calibrations, imageSize=shape, R=R, T=T, flags=cv2.CALIB_ZERO_DISPARITY)[:-2]
    # print(R1)
    # print(R12)
    # exit()
    # print()
    # print(R2)
    # print(R22)
    # exit()
    # print(P1)
    # print(P2)
    # print(Q)

    # rectify map for each head of the camera
    camera1_mapx, camera1_mapy = cv2.initUndistortRectifyMap(camera_calibrations['cameraMatrix1'], camera_calibrations['distCoeffs1'],
                                                            R1, P1, shape, cv2.CV_32FC1)
    camera2_mapx, camera2_mapy = cv2.initUndistortRectifyMap(camera_calibrations['cameraMatrix2'], camera_calibrations['distCoeffs2'],
                                                            R2, P2, shape, cv2.CV_32FC1)

    # R1_o = helpers.rodrigues(projection_matrix1['rvec'].copy())
    # t1_o = projection_matrix1['tvec'].copy()
    # P_large = np.eye(4)
    # P_large[:3, :] = np.column_stack((R1_o, t1_o))
    # inv_P_large = np.linalg.inv(P_large)
    # # Transforming Q to the coordinate system of the first camera
    # Q = inv_P_large @ Q

    # return maps and 23d matrix Q
    rectification = {
                      'map1x': camera1_mapx, 'map1y': camera1_mapy,
                      'map2x': camera2_mapx, 'map2y': camera2_mapy,
                      'Q': Q
                    }
    return rectification


def threedscan_single(camera1_image, camera2_image, config, rectification=None, directory=None, verbose=False):
    """
    If first two parameters are strings the parameters are assumed to be paths for the images
    otherwise the parameters should be the numpy images.
    Config should include information of the intrinsic camera calibrations and the calibration scene.
    If rectification is None directory must be given for the function to find the rectification.
    """

    if isinstance(camera1_image, str):
        camera1_image = io.imread(camera1_image)
    if isinstance(camera2_image, str):
        camera2_image = io.imread(camera2_image)

    if rectification is None and directory is None:
        raise ValueError('If rectification is None directory must not be None for the function to work')
    elif rectification is None and not directory is None:
        camera_calibrations = read_camera_calibrations(directory, config)
        rectification = find_rectification(camera1_image, camera2_image, config, camera_calibrations, verbose)

    # rectify images, i.e. all rows are the same in both cameras
    interpolation = cv2.INTER_LINEAR
    rectified_camera1_image = cv2.remap(camera1_image, rectification['map1x'], rectification['map1y'], interpolation)
    rectified_camera2_image = cv2.remap(camera2_image, rectification['map2x'], rectification['map2y'], interpolation)
    # plt.figure()
    # plt.imshow(rectified_camera1_image)
    # plt.figure()
    # plt.imshow(rectified_camera2_image)
    # plt.show()

    # estimate disparity map
    disparity = _disparity_matcher.compute(rectified_camera1_image, rectified_camera2_image).astype(np.float32) / 16.0

    # homemade cost function for segmentation
    # cost = ...
    segmentation = np.sum(rectified_camera1_image, axis=2) > 10
    disparity[~segmentation] = np.nan
    # disparity[disparity > camera1_image.shape[1]] = 0
    # plt.imshow(disparity)
    # plt.show()
    # exit()

    # 3D reconstruct points from disparity map
    threed_coordinates = cv2.reprojectImageTo3D(disparity, rectification['Q'])

    # write to 3D file
    print(threed_coordinates.shape)
    plt.imshow(helpers.threedimage_plotscale(threed_coordinates))
    plt.show()


def threedscan(directory, verbose=False):
    """
    Use function threedscan_single but for images in a full directory where calibration images are the only ones required with a scene for extrinsic calibration
    """
    images = helpers.listdir_images(directory)
    if len(images) <= 1:
        raise ValueError('At least two images from a stereo camera setup are required for the method to work.')

    # Config contains information of where camera calibration is and the scene geometry
    config_path = osp.join(directory, 'config.json')
    config = helpers.read_json(config_path)

    rectification = None
    calibration_images = sorted([im for im in images if 'calibrat' in im])
    if len(calibration_images) == 2:
        calibration_image_camera1 = io.imread(calibration_images[0], as_gray=True)
        calibration_image_camera2 = io.imread(calibration_images[1], as_gray=True)
        camera_calibrations = read_camera_calibrations(directory, config)
        rectification = find_rectification(calibration_image_camera1, calibration_image_camera2,
                camera_calibrations, config, verbose)

    camera1_images = sorted([im for im in images if 'camera1' in im and not 'calibrat' in im])
    camera2_images = sorted([im for im in images if 'camera2' in im and not 'calibrat' in im])

    for (im1, im2) in zip(camera1_images, camera2_images):
        threedscan_single(im1, im2, config, rectification, directory, verbose)
