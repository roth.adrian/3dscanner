# This is in seperate file since blender does not like to import many python packages
import numpy as np
import json

def sanitize_dict_rec(data, types_convert_dict, except_keys=[]):
    new_data = {}
    for key, value in data.items():
        if key in except_keys:
            new_data[key] = value
        elif isinstance(value, dict):
            new_data[key] = sanitize_dict_rec(value, types_convert_dict)
        else:
            converted = False
            for t, c in types_convert_dict.items():
                if isinstance(value, t):
                    new_data[key] = c(value)
                    converted=True
            if not converted:
                new_data[key] = value
    return new_data

def read_json(filepath):
    """
    Read a JSON file to a dictionary
    convert all lists to float64 ndarrays
    """
    with open(filepath) as f:
        content = f.readlines()
    data = json.loads("".join(content))
    data = sanitize_dict_rec(data, { list: np.float64 }, ['shape', 'scene'])
    return data

def write_json(filepath, data):
    """
    Write a dictionary to JSON format file
    convert all ndarrays to lists
    """
    data = sanitize_dict_rec(data, { np.ndarray: np.ndarray.tolist })
    content = json.dumps(data, sort_keys=True,
                         indent=4, separators=(',', ': ')) + "\n"
    with open(filepath, 'w') as f:
        f.write(content)


def rodrigues(rvec_or_R):
    """
    Changing rotation format from opencv rodrigues vector of 3 values to rotation matrix or vice versa
    See opencv documentation for Rodrigues function for further details
    """
    rvec_or_R = np.array(rvec_or_R)
    if rvec_or_R.size == 9:
        R = rvec_or_R
        # converting the rotation matrix to euler/rodrigues axis/angle form, rvec
        RTemp = (R - R.T) / 2
        rvec = np.array([RTemp[2, 1], RTemp[0, 2], RTemp[1, 0]])
        sin_theta = np.linalg.norm(rvec)
        cos_theta = (np.trace(R) - 1) / 2
        theta = np.arctan2(sin_theta, cos_theta)
        rvec *= theta / sin_theta
        return rvec
    elif rvec_or_R.size == 3:
        rvec = rvec_or_R
        theta = np.linalg.norm(rvec)
        rvec /= theta
        rx, ry, rz = rvec
        rvec = np.array([rvec])
        R = (np.cos(theta) * np.eye(3) +
            (1 - np.cos(theta)) * (rvec.T @ rvec) +
            np.sin(theta) * np.array([[  0, -rz,  ry],
                                      [ rz,   0, -rx],
                                      [-ry,  rx,   0]]))
        return R
    else:
        raise ValueError('Input array must be of either have 9 or 3 elements') 

def stereo_rectify(camera_calibrations, projection_matrix1, projection_matrix2):
    """
    input:
    projection matrices are dicts with rvec (rotation in rodrigues format) and tvec (translation vector of the cameras)
    output:
    R1 and R2 are rectification transforms for respective images, P is the final camera matrix for rectified cameras and Q is the image disparity transform to 3D coordinates if the first camera center is coordinate origin
    For horizontal configuration camera1 is assumed to be the left camera and for vertical camera 1 is the top camera.
    """
    # cameras original rotations and translations
    R1_o = rodrigues(projection_matrix1['rvec'].copy())
    R2_o = rodrigues(projection_matrix2['rvec'].copy())
    t1_o = projection_matrix1['tvec'].copy()
    t2_o = projection_matrix2['tvec'].copy()

    # camera centers
    C1 = -R1_o.T @ t1_o
    C2 = -R2_o.T @ t2_o

    diff_centres = C2 - C1
    # check if the cameras seem to be horizontal or vertical
    # horizontal if there is a larger change in Y coordinates compared to Z and vertical otherwise
    if diff_centres[1] > diff_centres[2]:
        # the vector from camera 1 to camera 2 is the new x direction of the camera
        ex = diff_centres
        # the cross product of both cameras' optical axes is the new y direction of the camera
        ey = np.cross(R2_o[-1, :], R1_o[-1, :])
    else:
        # for vertical cameras it is the other way around as the horizontal version
        ex = np.cross(R1_o[-1, :], R2_o[-1, :])
        ey = diff_centres
    # Scaling vectors to size 1
    ex /= np.linalg.norm(ex)
    ey /= np.linalg.norm(ey)

    # the new z direction is cross product of x and y
    ez = np.cross(ex, ey)

    # rectified rotation matrix
    R = np.vstack((ex, ey, ez))

    # Rotation for cameras to get rectified
    R1 = R @ R1_o.T
    R2 = R @ R2_o.T
    # Trick to avoid translation of images in rectification
    # R1[[0, 1], 2] = 0
    # R2[[0, 1], 2] = 0

    # new translation vector of the rectified camera
    # t = (t1_o + t2_o) / 2
    K = camera_calibrations['cameraMatrix1']
    K[0, 1] = 0  # setting new skew to zero
    # translation vector between cameras in the first camera coordinate system
    translation_vector = R1_o @ diff_centres
    translation_vector[2] = 0  # I know that either camera X ot X is non zero, Z is zero
    P1 = K @ np.column_stack((np.eye(3), np.zeros(3)))
    P2 = K @ np.column_stack((np.eye(3), translation_vector))

    ## Estimating the disparity to threed matrix, assuming that both cameras have the same intrinsic parameters
    # distance between the camera centers
    b = np.linalg.norm(C2 - C1)
    # focal lengths and principal_point
    fu, fv, pu, pv = camera_calibrations['cameraMatrix1'].flatten()[[0, 4, 2, 5]]
    # the projection matrix, Q * [u, v, disparity, 1]^T = 3D estimation
    Q = np.array([[1,     0,    0, -pu],
                  [0, fv/fu,    0, -pv],
                  [0,     0,    0,  fu],
                  [0,     0, fu/b,   0]])
    # transforming Q from the first camera coordinate system to the global one
    P_large = np.eye(4)
    P_large[:3, :] = np.column_stack((R1_o, t1_o))
    inv_P_large = np.linalg.inv(P_large)
    Q = inv_P_large @ Q

    return R1, R2, P1, P2, Q
