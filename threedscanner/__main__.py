import argparse

def camera_calibration(args):
    from . import camera_calibration
    camera_calibration.calibrate_camera(**vars(args))

def swept_planes(args):
    from . import swept_planes
    swept_planes.threedscan(**vars(args))

def stereo_disparity(args):
    from . import stereo_disparity
    stereo_disparity.threedscan(**vars(args))

def simulation(args):
    from . import simulation
    simulation.run(**vars(args))

def main():
    parser = argparse.ArgumentParser(description='Module for performing postproccesing for different 3Dscanning methods.')
    subparsers = parser.add_subparsers()


    calibration_parser = subparsers.add_parser('camera-calibration', help='Module for calibrating camera using opencv function.')
    calibration_parser.add_argument('calibration_location', type=str, 
                                    help='The calibration_directory with calibration images of a chessboard with different orientation in each image or a scene to get extrinsic calibration of.')
    calibration_parser.add_argument('--extrinsic', action='store_true',
                                    help='Do extrinsic calibration of a scene.')
    calibration_parser.add_argument('--verbose', '-v', action='store_true',
                                    help='Print more')
    calibration_parser.set_defaults(func=camera_calibration)


    swept_planes_parser = subparsers.add_parser('swept-planes', help='Module for 3D scanning using the swept planes procedure.')
    swept_planes_parser.add_argument('directory', type=str, 
                                    help='Should include a file called config.json with information of location of camera calibration and scene geometry together with images of the scanning. A video can also be used but then moviepy should be installed to extract the frames from this video automatically.')
    swept_planes_parser.add_argument('--laser', action='store_true',
                                    help='Assume that a laser was used for the sweep')
    swept_planes_parser.add_argument('--verbose', '-v', action='store_true',
                                    help='Print more')
    swept_planes_parser.set_defaults(func=swept_planes)


    stereo_disparity_parser = subparsers.add_parser('stereo-disparity', help='Module for 3D scanning using the stereo-disparity procedure.')
    stereo_disparity_parser.add_argument('directory', type=str, 
                                    help='Should include a file called config.json with information of location of camera calibration and scene geometry together with images from the stereo setup.')
    stereo_disparity_parser.add_argument('--verbose', '-v', action='store_true',
                                    help='Print more')
    stereo_disparity_parser.set_defaults(func=stereo_disparity)


    simulation_parser = subparsers.add_parser('simulate', help='Module for simulating the problems to attempt solving them if no arguments is given all simulations are run.')
    simulation_parser.add_argument('--camera-calibration', '-cc', action='store_true',
                                   help='Simulate the camera.')
    simulation_parser.add_argument('--swept-planes', '-sp', action='store_true',
                                   help='Simulate the swept planes scanning.')
    simulation_parser.add_argument('--stereo-disparity', '-sd', action='store_true',
                                   help='Simulate the stereo disparity scanning.')
    simulation_parser.add_argument('--args', nargs=argparse.REMAINDER, default=[],
                                   help='Optional extra commands passed to the simulation, such as output directory, number of images etc.')
    simulation_parser.set_defaults(func=simulation)

    args = parser.parse_args()
    func = args.func
    del args.func
    func(args)

if __name__ == '__main__':
    main()
