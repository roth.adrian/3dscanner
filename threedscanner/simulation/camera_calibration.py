""" 
Script to simulate camera simulation images
is uses the prepared blender file in camera_simulation.blend
"""
import numpy as np
import os
import os.path as osp

import sys
sys.path.insert(0, osp.join(osp.dirname(__file__), '../../'))
import threedscanner.helpers_base as helpers
from threedscanner.simulation import helpers as simulation_helpers 

def main():
    ####### Parse arguments ########
    sys.argv = simulation_helpers.prep_argv_for_parsing(sys.argv)

    parser = simulation_helpers.default_parser('simulated_camera', 'Simulate calibration images for a camera using blender')
    args = parser.parse_args()

    ####### Smooth fix of running blender with this file ####
    file_directory = osp.dirname(__file__)
    scene_file = osp.join(file_directory, 'camera_calibration.blend')
    bpy = simulation_helpers.run_python_blender_script(__file__, scene_file)
    # This is after the subprocess with blender is done
    if bpy is None:
        return

    output_dir = args.output_directory
    os.makedirs(output_dir, exist_ok=True)
    empty = bpy.data.objects['Empty']
    scene = bpy.data.scenes['Scene']

    scene.render.image_settings.file_format = 'PNG'
    # scene.render.image_settings.color_mode = 'BW'

    calibration_path = lambda i: osp.join(output_dir, 'calibration_{0:03d}.png'.format(i))

    scene.render.filepath = calibration_path(0)
    bpy.ops.render.render(write_still=True)
    for i in range(1, args.n_images):
        # Random rotation of the camera around the empty in the centre
        empty.rotation_euler = ((np.random.random() * 100 - 50) * np.pi / 180,
                                (np.random.random() * 100 - 50) * np.pi / 180,
                                0)
        scene.render.filepath = calibration_path(i)
        bpy.ops.render.render(write_still=True)


    true_calibration = { 'cameraMatrix': simulation_helpers.get_calibration_matrix_K_from_blender(scene),
                         'distCoeffs': np.zeros(5) }
    helpers.write_json(osp.join(output_dir, 'calibration.json'), true_calibration)

    # Writing config of size and shape of the chessboard grid used
    config_grid_path = osp.join(file_directory, '../../grids/chessboard_config.json')
    config_grid = helpers.read_json(config_grid_path)
    helpers.write_json(osp.join(output_dir, 'config.json'), config_grid)


if __name__ == '__main__':
    main()
