""" 
Script to simulate a swept planes 3D scanning
is uses the prepared blender file in swept_planes.blend
"""
import os
import os.path as osp

import sys
sys.path.insert(0, osp.join(osp.dirname(__file__), '../../'))
import threedscanner.helpers_base as helpers
from threedscanner.simulation import helpers as simulation_helpers 

def main():
    ####### Parse arguments ########
    sys.argv = simulation_helpers.prep_argv_for_parsing(sys.argv)

    parser = simulation_helpers.default_parser('simulated_swept_planes', 'Simulate swept-planes scanning using blender')
    args = parser.parse_args()

    ####### Smooth fix of running blender with this file ####
    file_directory = osp.dirname(__file__)
    scene_file = osp.join(file_directory, 'swept_planes.blend')
    bpy = simulation_helpers.run_python_blender_script(__file__, scene_file)
    # This is after the subprocess with blender is done
    if bpy is None:
        return

    output_dir = args.output_directory
    os.makedirs(output_dir, exist_ok=True)

    bpy.data.scenes['Scene'].render.image_settings.file_format = 'AVI_JPEG'
    bpy.data.scenes['Scene'].render.filepath = osp.join(output_dir, 'swept_planes.avi')
    bpy.ops.render.render(animation=True)

    radius = bpy.data.objects['Camera'].location[-1]
    config = { 'camera': '../simulated_camera/calibration.json',
               'scene': simulation_helpers.scene,
               'real_projection_matrix': simulation_helpers.projection_camera_matrix(0, radius) }
    helpers.write_json(osp.join(output_dir, 'config.json'), config)

if __name__ == '__main__':
    main()
