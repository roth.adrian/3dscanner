import sys

def run(args, camera_calibration=False, swept_planes=False, stereo_disparity=False):
    """ If all are false all simulations are run """
    # Removing possiple arguments that the simulations will be angry at since I have done a hack for running them with blender
    sys.argv = [__file__] + args

    # if all or none of the above then all simulations are run
    none = (not camera_calibration) and (not swept_planes) and (not stereo_disparity)

    if camera_calibration or none:
        from . import camera_calibration
        camera_calibration.main()
    if swept_planes or none:
        from . import swept_planes
        swept_planes.main()
    if stereo_disparity or none:
        from . import stereo_disparity
        stereo_disparity.main()
