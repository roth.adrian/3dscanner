""" 
Script to simulate a stereo disparity 3D scanning
is uses the prepared blender file in stereo_disparity.blend
"""
import os
import os.path as osp
import numpy as np

import sys
sys.path.insert(0, osp.join(osp.dirname(__file__), '../../'))

import threedscanner.helpers_base as helpers
from threedscanner.simulation import helpers as simulation_helpers 

def main():
    ####### Parse arguments ########
    sys.argv = simulation_helpers.prep_argv_for_parsing(sys.argv)

    parser = simulation_helpers.default_parser('simulated_stereo_disparity', 'Simulate swept-planes scanning using blender')
    args = parser.parse_args()

    ####### Smooth fix of running blender with this file ####
    file_directory = osp.dirname(__file__)
    scene_file = osp.join(file_directory, 'stereo_disparity.blend')
    bpy = simulation_helpers.run_python_blender_script(__file__, scene_file)
    # This is after the subprocess with blender is done
    if bpy is None:
        return

    output_dir = args.output_directory
    os.makedirs(output_dir, exist_ok=True)

    bpy.data.scenes['Scene'].render.image_settings.file_format = 'PNG'
    # bpy.data.scenes['Scene'].render.image_settings.color_mode = 'BW'

    camera = bpy.data.objects['Camera']
    camera_empty = camera.parent
    radius = camera.location[-1]
    # lamp = bpy.data.objects['Lamp']

    camera_angles = [-2, 2]  # degrees
    calc_camera_rotation = lambda rot: np.array([90, 0, 90 + rot]) * np.pi / 180
    def take_stereo_image(i):
        camera_empty.rotation_euler = calc_camera_rotation(camera_angles[0])
        bpy.data.scenes['Scene'].render.filepath = osp.join(output_dir, 'camera1_{}.png'.format(i))
        bpy.ops.render.render(write_still=True)

        camera_empty.rotation_euler = calc_camera_rotation(camera_angles[1])
        bpy.data.scenes['Scene'].render.filepath = osp.join(output_dir, 'camera2_{}.png'.format(i))
        bpy.ops.render.render(write_still=True)


    # Stereo Calibration images
    bpy.data.objects['Sphere'].hide_render = True
    bpy.data.objects['Plane'].hide_render = False
    bpy.data.objects['Plane.001'].hide_render = False
    take_stereo_image('stereo_calibrate')

    # Empty images maybe used to find foreground in future
    bpy.data.objects['Plane'].hide_render = True
    bpy.data.objects['Plane.001'].hide_render = True
    # take_stereo_image('empty_calibrate')

    # Image with only the sphere
    bpy.data.objects['Sphere'].hide_render = False
    take_stereo_image(0)

    
    projection_matrix1 = simulation_helpers.projection_camera_matrix(camera_angles[0], radius)
    projection_matrix2 = simulation_helpers.projection_camera_matrix(camera_angles[1], radius)

    # K = simulation_helpers.get_calibration_matrix_K_from_blender(bpy.data.scenes['Scene'])
    # camera_calibration = { 'cameraMatrix': K, 'distCoeffs': np.zeros(5) }
    # camera_calibrations = {}
    # [camera_calibrations.update({'{}{}'.format(key, i): value for key, value in camera_calibration.items() }) for i in range(2)]

    # correct 3D estimation matrix from disparity
    # R1, R2, P, Q = helpers.stereo_rectify(camera_calibrations, projection_matrix1, projection_matrix2, 'horizontal')

    config = { 'camera1': '../simulated_camera/calibration.json',
               'camera2': '../simulated_camera/calibration.json',
               'scene': simulation_helpers.scene,
               'real_projection_matrix1': projection_matrix1,
               'real_projection_matrix2': projection_matrix2 }
    helpers.write_json(osp.join(output_dir, 'config.json'), config)

if __name__ == '__main__':
    main()
