import argparse
import numpy as np
import sys

import threedscanner.helpers_base as helpers

scene = [1130, 858]  # size of simulation scene box, last two dimensions are equal

def default_parser(name, description='', parser=None):
    if parser is None:
        parser = argparse.ArgumentParser(description=description)
    parser.add_argument('--output-directory', type=str, default='data/{}'.format(name),
                        help='Name for the camera, default simulated_camera.')
    parser.add_argument('--n-images', type=int, default=15,
                        help='Number of calibration images from the simulation, will not always be used')
    return parser

def prep_argv_for_parsing(argv):
    if '--' in argv:
        # find out the python script that is running
        f = argv[argv.index('--python') + 1]
        # Remove blender arguments
        argv = argv[argv.index('--') + 1:]
        # add the regular arguments
        argv = [f] + argv
    return argv

def run_python_blender_script(script, scene=None):
    try:
        import bpy
        return bpy
    except ImportError:
        import subprocess
        argv = sys.argv
        # removing program and script name from argv
        argv = argv[1:]

        call_base = ['blender', '--background']
        if not scene is None:
            call_base.append(scene)
        call_list = call_base + ['--python', script, '--'] + argv
        subprocess.call(call_list)
        return None


# Adapted from code at:
# https://blender.stackexchange.com/questions/38009/3x4-camera-matrix-from-blender-camera#38210
def get_calibration_matrix_K_from_blender(scene):
    camera_data = scene.camera.data
    f_in_mm = camera_data.lens
    resolution_x_in_px = scene.render.resolution_x
    resolution_y_in_px = scene.render.resolution_y
    scale = scene.render.resolution_percentage / 100
    pixel_aspect_ratio = scene.render.pixel_aspect_x / scene.render.pixel_aspect_y
    if (camera_data.sensor_fit == 'VERTICAL'):
        # the sensor height is fixed (sensor fit is horizontal), 
        # the sensor width is effectively changed with the pixel aspect ratio
        sensor_height_in_mm = camera_data.sensor_height
        s_v = resolution_y_in_px * scale / sensor_height_in_mm
        s_u = s_v / pixel_aspect_ratio
    else: # 'HORIZONTAL' and 'AUTO'
        # the sensor width is fixed (sensor fit is horizontal), 
        # the sensor height is effectively changed with the pixel aspect ratio
        sensor_width_in_mm = camera_data.sensor_width
        s_u = resolution_x_in_px * scale / sensor_width_in_mm
        s_v = s_u * pixel_aspect_ratio

    # Parameters of intrinsic calibration matrix K
    alpha_u = f_in_mm * s_u
    alpha_v = f_in_mm * s_v
    u_0 = resolution_x_in_px * scale / 2
    v_0 = resolution_y_in_px * scale / 2
    skew = 0 # only use rectangular pixels

    K = np.array([[alpha_u, skew,    u_0],
                  [0,       alpha_v, v_0],
                  [0,       0,       1  ]])
    return K

def projection_camera_matrix(rot_z, radius):
    """ Calculate extrinsic camera matrix from a camera in xy-plane at radius with z-rotation in degrees looking at the origin """
    rot_z = rot_z * np.pi / 180
    loc = np.array([np.cos(rot_z)*radius, np.sin(rot_z)*radius, 0]) * 1000  # estimate location in mm
    z = -loc / np.linalg.norm(loc)
    y = np.array([0, 0, -1])
    y = y - np.sum(y * z) * z
    y /= np.linalg.norm(y)
    x = np.cross(y, z).T
    R = np.vstack((x, y, z))  # rotation matrix for camera
    tvec = -np.sum(R * loc, axis=1)  # translation vector for camera
    # converting the rotation matrix to euler/rodrigues axis/angle form, rvec
    rvec = helpers.rodrigues(R)
    return { 'rvec': rvec, 'tvec': tvec }
