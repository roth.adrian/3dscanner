#!/usr/bin/python3
import cv2
import glob
import numpy as np
import numpy.linalg as lin
import os
import sys
import yaml


with open(sys.argv[2]) as f:
    grid = yaml.safe_load(f)
grid_width = grid['grid_width']
size = tuple(grid['size'])

# termination criteria
criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)

objp = np.zeros((size[0] * size[1], 3), np.float32)
objp[:, [2, 1]] = np.mgrid[0:size[0], 0:size[1]].T.reshape(-1, 2) * grid_width
objp[:, [2, 1]] -= np.tile((np.array(size) - 1) * grid_width / 2, (size[0] * size[1], 1))

def estimate_projection_matrix(file, K, dc):
    img = cv2.imread(file)

    # undistort
    h,  w = img.shape[:2]
    new_K, roi = cv2.getOptimalNewCameraMatrix(K, dc, (w, h), 1, (w, h))
    dst = cv2.undistort(img, K, dc, None, new_K)
    x, y, w, h = roi
    dst = dst[y:y+h, x:x+w]
    gray = cv2.cvtColor(dst, cv2.COLOR_BGR2GRAY)

    # Find the chess board corners
    ret, corners = cv2.findChessboardCorners(gray, size, None)
    # If found, add object points, image points (after refining them)
    if ret == True:
        cv2.cornerSubPix(gray, corners, (11,11), (-1,-1), criteria)

        # Draw and display the corners
        cv2.drawChessboardCorners(img, size, corners, ret)
        cv2.imshow('img', img)
        cv2.waitKey(500)

        _, rvec, tvec = cv2.solvePnP(objp, corners, K, dc)
        R, _ = cv2.Rodrigues(rvec)
        return np.hstack((np.mat(R), np.mat(tvec)))
    else:
        sys.exit('Could not find chessboard in ' + file)

############# Main ##########################

dir = sys.argv[1]
last_dir, _ = os.path.split(dir)
last_dir += '/'

with open(dir + '/config.yml') as f:
    config = yaml.safe_load(f)


with open(last_dir + config['camera1'] + '/calibration.yml') as f:
    cam1 = yaml.safe_load(f)
K1 = np.mat(cam1['intrinsic_camera_matrix'])
dc1 = np.mat(cam1['distortion_coefficients'])

with open(last_dir + config['camera2'] + '/calibration.yml') as f:
    cam2 = yaml.safe_load(f)
K2 = np.mat(cam2['intrinsic_camera_matrix'])
dc2 = np.mat(cam2['distortion_coefficients'])


C1 = estimate_projection_matrix(dir + '/cam1_stereo_calibrate.png', K1, dc1)
C2 = estimate_projection_matrix(dir + '/cam2_stereo_calibrate.png', K2, dc2)

if "simulate" in dir:  # Simulated images
    print('Simulated directory, checking estimation')
    err1 = lin.norm(C1[:, :3] - np.mat(config['projection_matrix1'])[:, :3]) / lin.norm(C1[:, :3]) + \
           lin.norm(np.abs(C1[:, 3]) - np.abs(np.mat(config['projection_matrix1'])[:, 3])) / lin.norm(C1[:, 3])
    print('Error camera matrix 1 = ' + str(err1))
    err2 = lin.norm(C2[:, :3] - np.mat(config['projection_matrix2'])[:, :3]) / lin.norm(C2[:, :3]) + \
           lin.norm(np.abs(C2[:, 3]) - np.abs(np.mat(config['projection_matrix2'])[:, 3])) / lin.norm(C2[:, 3])
    print('Error camera matrix 2 = ' + str(err2))
    C1 = np.mat(config['projection_matrix1'])
    C2 = np.mat(config['projection_matrix2'])

stereo_camera_calibration = dict(projection_matrix1 = C1.tolist(),
                                 intrinsic_camera_matrix1 = K1.tolist(),
                                 distortion_coefficients1 = dc1.tolist(),
                                 projection_matrix2 = C2.tolist(),
                                 intrinsic_camera_matrix2 = K2.tolist(),
                                 distortion_coefficients2 = dc2.tolist())
with open(dir + '/stereo_camera_calibration.yml', 'w') as outfile:
    yaml.dump(stereo_camera_calibration, outfile)
