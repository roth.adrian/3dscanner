#!/usr/bin/python3
import cv2
import glob
import numpy as np
import numpy.linalg as lin
import os
import sys
import yaml
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D




def flood(im):
    im_floodfill = im.copy()
    h, w = im.shape[:2]
    mask = np.zeros((h + 2, w + 2), np.uint8)

    cv2.floodFill(im_floodfill, mask, (0, 0), 255)
    cv2.floodFill(im_floodfill, mask, (w - 1, 0), 255)
    cv2.floodFill(im_floodfill, mask, (0, h - 1), 255)
    cv2.floodFill(im_floodfill, mask, (w - 1, h - 1), 255)
    im_floodfill_inv = cv2.bitwise_not(im_floodfill)
    return im | im_floodfill_inv


file = sys.argv[1]
dir = os.path.dirname(file)

with open(file) as f:
    stereo_camera_calibration = yaml.safe_load(f)
C1 = np.mat(stereo_camera_calibration['projection_matrix1'])
K1 = np.mat(stereo_camera_calibration['intrinsic_camera_matrix1'])
dc1 = np.mat(stereo_camera_calibration['distortion_coefficients1'])
C2 = np.mat(stereo_camera_calibration['projection_matrix2'])
K2 = np.mat(stereo_camera_calibration['intrinsic_camera_matrix2'])
dc2 = np.mat(stereo_camera_calibration['distortion_coefficients2'])

# Calculate distance (b) between cameras
T1 = C1[:, :3].T * C1[:, 3]
T2 = C2[:, :3].T * C2[:, 3]
b = lin.norm(T2 - T1)
dZ = K1[0, 0] / 2 * b

# Q is matrix for conversion from 2D disparity points to 3D
Q = np.mat(np.zeros((4, 4)))
Q[:3, [0, 1, 3]] = lin.inv(K1)
Q[3, 2] = 1 / dZ

# min_disp = int(dZ / 1300)
# min_disp = 0
# max_disp = 100
# num_disp = int(int(max_disp - min_disp) / 16) * 16
# print(max_disp)
# window_size = 11
# matcher = cv2.StereoBM(cv2.STEREO_BM_BASIC_PRESET, 0, window_size)


# disparity range is tuned for 'aloe' image pair
window_size = 3
min_disp = 32
num_disp = 112 - min_disp
matcher = cv2.StereoSGBM(
    minDisparity = min_disp,
    numDisparities = num_disp,
    SADWindowSize = window_size,
    P1 = 8*3*window_size**2,
    P2 = 32*3*window_size**2,
    disp12MaxDiff = 1,
    uniquenessRatio = 10,
    speckleWindowSize = 100,
    speckleRange = 32
)

background = cv2.imread(dir + '/rectified_cam1_empty_calibrate.png', 0)

images_cam1 = sorted(glob.glob(dir + '/rectified_cam1_*.png'))
images_cam2 = sorted(glob.glob(dir + '/rectified_cam2_*.png'))
images = zip(images_cam1, images_cam2)

for cam1, cam2 in images:
    if not "calibrate" in cam1:
        img1 = cv2.imread(cam1, 0)
        img2 = cv2.imread(cam2, 0)
        mask = flood((np.abs(img1 - background) > 5).astype(np.uint8) * 255)
        disp = (matcher.compute(img1, img2) / 16)
        [x, y] = np.nonzero(mask)
        p = np.mat(np.vstack((x, y, disp[x, y], np.ones(x.size))))
        P = Q * p
        P /= np.repeat(P[3:4, :], 4, axis=0)

        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        ax.scatter(P[0, :], P[1, :], P[2, :], c='r')
        plt.show()

        # plt.figure()
        # plt.imshow(disp, cmap='gray')
        # plt.colorbar()
        # plt.figure()
        # plt.imshow(disp & mask, cmap='gray')
        # plt.colorbar()
        # plt.show()
