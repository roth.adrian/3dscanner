#!/usr/bin/python3
import cv2
import glob
import numpy as np
import os
import sys
import yaml

file = sys.argv[1]
dir = os.path.dirname(file)

with open(file) as f:
    stereo_camera_calibration = yaml.safe_load(f)
C1 = np.mat(stereo_camera_calibration['projection_matrix1'])
K1 = np.mat(stereo_camera_calibration['intrinsic_camera_matrix1'])
dc1 = np.mat(stereo_camera_calibration['distortion_coefficients1'])
C2 = np.mat(stereo_camera_calibration['projection_matrix2'])
K2 = np.mat(stereo_camera_calibration['intrinsic_camera_matrix2'])
dc2 = np.mat(stereo_camera_calibration['distortion_coefficients2'])


def find_rectify_homography(C1, C2):
    T1 = C1[:3, :3].T * C1[:, 3]
    T2 = C2[:3, :3].T * C2[:, 3]
    B = T2 - T1
    x = (C1[0, :3] * B)[0, 0] * B
    x /= np.linalg.norm(x)
    z = C1[2, :3].T - (C1[2, :3] * x)[0, 0] * x
    z /= np.linalg.norm(z)
    y = np.cross(z.T, x.T).T
    H = np.hstack((x, y, z)).T
    H1 = H * C1[:3, :3].T
    H2 = H * C2[:3, :3].T
    H1[[0, 1], 2] = 0
    H2[[0, 1], 2] = 0
    return H1, H2

def undistort_rectify_images(images, K, dc, H):
    img = cv2.imread(images1[0])
    size = img.shape[:2][::-1]

    new_K, roi = cv2.getOptimalNewCameraMatrix(K, dc, size, 1, size)
    x, y, w, h = roi
    mapx, mapy = cv2.initUndistortRectifyMap(K, dc, H, new_K, size, cv2.CV_32FC1)
    for f in images:
        img = cv2.imread(f)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        dst = cv2.remap(img, mapx, mapy, cv2.INTER_LINEAR)
        dst = dst[y:y+h, x:x+w]

        if "stereo_calibrate" in f:
            n = int(size[1] / 20)
            p1 = np.vstack((np.ones(n) * 0, np.linspace(0, size[1], n)))
            p2 = np.vstack((np.ones(n) * size[0], np.linspace(0, size[1], n)))
            lines = np.vstack((p1, p2)).T.astype(np.int)
            for l in lines:
                cv2.line(dst, tuple(l[:2]), tuple(l[2:]), (0, 0, 255))

        dir, file = os.path.split(f)
        cv2.imwrite(dir + '/rectified_' + file, dst)


############# Main ##########################

H1, H2 = find_rectify_homography(C1, C2)

images1 = glob.glob(dir + '/cam1*.png')
undistort_rectify_images(images1, K1, dc1, H1)
images2 = glob.glob(dir + '/cam2*.png')
undistort_rectify_images(images2, K2, dc2, H2)
