import cv2
import numpy as np
import os
import os.path as osp
from skimage import io, feature, filters, transform, morphology, draw
import matplotlib.pyplot as plt

# Just importing for use here
from .helpers_base import read_json, write_json, rodrigues, stereo_rectify
read_json; write_json, rodrigues, stereo_rectify


image_extensions = ['.png', '.tif', '.tiff', '.jpg']
def listdir_images(directory):
    return listdir(directory, image_extensions)

def listdir(directory, ext=None):
    """
    List all files in directory sorted
    optionally only include files with a certain extension (include '.' in the extension name)
    if ext is a list files with possible multiple extensions are returned
    """
    files = os.listdir(directory)
    output_files = []
    if ext is None:
        exts = []
        output_files = files
    elif isinstance(ext, list):
        exts = ext
    else:
        exts = [ext]
    for ext in exts:
        output_files.extend([f for f in files if osp.splitext(f)[1] == ext])
    return np.sort([osp.join(directory, f) for f in output_files])

def split_clip(movie_path):
    """
    Extract all frames in a movie into the same directory as the movie file
    """
    # Moviepy is not a dependency for the package but can be used for simplicity of inserting movie directly
    try:
        from moviepy.editor import VideoFileClip
    except ImportError:
        raise ImportError('If you use a movie as input you need to install moviepy, otherwise you can split the video into frames using imagemagick or similar before running 3D scanner')
    clip = VideoFileClip(movie_path)
    base = osp.splitext(movie_path)[0]

    for i, frame in enumerate(clip.iter_frames(dtype=np.uint8)):
        io.imsave('{0:s}_frame{1:03d}.png'.format(base, i), frame)

def calculate_scene_points(scene, return_intersection_point=False):
    """
    Using a calibration scene as two orthogonal planes with four calibration points (shown as small 2x2 chessboard squares).
    The world coordinate system has origin at the center of the block that the planes together with calibration points form and positive z-axis directly looking away from the intersection line of the two planes.
    scene is either vector of length two or three where scene = [d1, d2(, d3)]
    d1 is the distance between the calibration points along the intersection line of the planes,
    d2 is the distance from the intersection to the left calibration points (when scene is standing up),
    d3 is the distance from the intersection to the right calibration points.
    If d3 is None it defaults to d2

    All distances should have the same unit of length.

    An intersection point of the calibration planes can also be added in the return values.
    """
    # prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
    shape = scene['shape']
    square_width = scene['square_width']
    objp = np.zeros((shape[0] * shape[1], 3), np.float32)
    objp[:, :2] = np.mgrid[0:shape[0], 0:shape[1]].T.reshape(-1, 2) * square_width
    return objp

    # if len(scene) == 2:
    #     scene.append(scene[-1])
    # elif len(scene) > 3 or len(scene) < 2:
    #     raise ValueError('Scene parameter should be either 2 or 3 elements long')

    # d1, d2, d3 = scene

    # # diagonal distance between d2 and d3, middle of this diagonal
    # # at the middle of d1 is the origin of the coordinate system
    # dd = np.sqrt(d2**2 + d3**2)

    # # Four points with cartesian coordinates
    # scene_points = []
    # scene_points.append([0, -dd/2,  d1/2])
    # scene_points.append([0,  dd/2,  d1/2])
    # scene_points.append([0, -dd/2, -d1/2])
    # scene_points.append([0,  dd/2, -d1/2])
    # scene_points = np.float32(scene_points)
    # # adding single axis as is used in openCV
    # scene_points = np.expand_dims(scene_points, 1)

    # if return_intersection_point:
    #     # trigonomotry calculated this
    #     alpha = np.pi / 2 + 2 * np.arcsin(d3 / dd)
    #     intersection_point = np.array([[[np.cos(alpha), np.sin(alpha), 0]]]) * dd / 2

    #     return scene_points, intersection_point
    # else:
    #     return scene_points

def find_calibration_point(points):
    """
    find which of the points is the one used for calibration
    also the orientation of the point is returned that is true if the squares looks like forward slash and false if more like backward slash
    """
    n_points = points.shape[0]
    if n_points < 7:
        raise ValueError('Too few corners found for estimation of calibration point')

    # Looking at distances between the points
    inds = np.arange(n_points)
    rows, cols = np.meshgrid(inds, inds, indexing='ij')
    distance_matrix = np.sqrt((points[rows, 0] - points[cols, 0])**2 + (points[rows, 1] - points[cols, 1])**2)
    distance_matrix_sorted = np.sort(distance_matrix, axis=1)[:, 1:] # first row are just zeros

    # four closest should be equally close and
    # the next two should also be equally close with distance around \sqrt(2) * (distance of first four)
    feature_vector = (
                        np.std(distance_matrix_sorted[:, :4], axis=1)
                      + np.std(distance_matrix_sorted[:, 4:6], axis=1)
                      )
    calibration_point_ind = np.argmin(feature_vector)

    # finding the rotation of the calibration point using the two points fifths and sixths closest to the centre
    rot_point_inds = np.argsort(distance_matrix[calibration_point_ind, :])[5:7]  # zeroth is just zero so incrementing the index
    rotation = np.arctan(np.diff(points[rot_point_inds, 0]) / np.diff(points[rot_point_inds, 1]))[0] - np.pi / 4

    return points[calibration_point_ind, :], rotation


def find_scene(calibration_image, automatic=False, verbose=False):
    """
    Find the four "chessboard" corners that are assumed to be in the scene.
    look at the file grid/stage.png in the repository to see half of the stage with two calibration points.
    Also the rotation of the calibration background is estimated to order the points from top left to bottom right for a calibration scene in standing position

    The retured image points have a shape 4x2 where the first column is the x coordinate and second is y

    If automatic the algorithm tries to find scene without manual input (does NOT work well) otherwise the user will click in all points.
    """
    if len(calibration_image.shape) == 3:
        # to grayscale
        calibration_image = cv2.cvtColor(calibration_image, cv2.COLOR_BGR2GRAY)
    mid = np.array(calibration_image.shape) // 2
    if automatic: 

        # blurring_size = 201
        # blurring_factor = int(np.min(calibration_image.shape) / blurring_size)

        # calibration_image = calibration_image / np.max(calibration_image)
        # calibration_image = exposure.equalize_hist(calibration_image)
        # k = 0
        # blurring_size = int(np.min(calibration_image.shape) * (0.2 if k % 2 == 0 else 0.1) / 2) * 2 + 1
        # print(blurring_size)

        # adaptive_threshold = signal.convolve2d(calibration_image, np.ones((blurring_size,)*2), mode='same')
        # plt.imshow(adaptive_threshold)
        # plt.show()
        # exit()
        # calibration_binary = cv2.adaptiveThreshold(calibration_image.astype(np.uint8), 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, blurring_size, (k/2)*5);
        # calibration_binary = filters.threshold_niblack(calibration_image, window_size=blurring_size) > calibration_image

        # downscaled = transform.downscale_local_mean(calibration_image, (blurring_factor,)*2)
        max_size = 2**9  # pixels
        image_min_size = np.min(calibration_image.shape)
        if image_min_size > max_size * 2:
            downsample_factor = image_min_size // max_size
            calibration_image = transform.downscale_local_mean(calibration_image, (downsample_factor,)*2)
            image_min_size /= downsample_factor
        window_size = int(image_min_size * 0.1)
        window_size += 1 - window_size % 2  # make it an odd number
        calibration_binary = filters.threshold_niblack(calibration_image, window_size=window_size) < calibration_image  # segment the image
        # print(window_size // 8)
        calibration_binary = morphology.opening(calibration_binary, np.ones((4, 4)))

        plt.imshow(calibration_binary)
        plt.show()
        exit()


        # plt.imshow(calibration_image)
        # plt.show()
        # exit()

        # Find potential corners using harris algorithm
        harris_map = feature.corner_harris(calibration_binary)
        corner_pixels = feature.corner_peaks(harris_map, min_distance=4, threshold_rel=0.1, num_peaks=80)
        corners = feature.corner_subpix(calibration_image, corner_pixels, window_size=13)

        if verbose:
            # print(np.min(calibration_image), np.max(calibration_image))
            # print(np.min(harris_map), np.max(harris_map))
            # print(harris_map.shape)
            plt.imshow(harris_map)
            plt.plot(corners[:, 1], corners[:, 0], 'r*')
            plt.show()
            exit()

        if len(corners) < 4:
            raise ValueError('Too few corners found.')

        # partition the corners into four parts, one calibration point per partition is assumed
        partitioned_corners = []
        # top left
        partitioned_corners.append(corners[(corners[:, 0] < mid[0]) & (corners[:, 1] < mid[1]), :])
        # top right
        partitioned_corners.append(corners[(corners[:, 0] < mid[0]) & (corners[:, 1] >= mid[1]), :])
        # lower left
        partitioned_corners.append(corners[(corners[:, 0] >= mid[0]) & (corners[:, 1] < mid[1]), :])
        # lower right
        partitioned_corners.append(corners[(corners[:, 0] >= mid[0]) & (corners[:, 1] >= mid[1]), :])

        calibration_points = [find_calibration_point(pc) for pc in partitioned_corners]
        scene_rotation = np.mean([rot for _, rot in calibration_points]) 
        calibration_points = [calibration_point for calibration_point, _ in calibration_points]
        # changing from row column to x y order
        calibration_points = calibration_points[:, ::-1]
    else:
        plt.imshow(calibration_image)
        plt.title('Click on calibration_points.')
        calibration_points = np.array(plt.ginput(4))
        plt.title('Click rotation for single calibration point')
        rotation_points = np.array(plt.ginput(2))
        plt.close()
        scene_rotation = np.arctan2(rotation_points[1, 1] - rotation_points[0, 1], rotation_points[1, 0] - rotation_points[0, 0])
        scene_rotation = np.arctan(np.diff(rotation_points[:, 1]) / np.diff(rotation_points[:, 0]))[0] - np.pi / 4

    # Rotation below zero means that the calibration plane is standing and otherwise lying.
    # The calibration points should be ordered in lying position from topleft to bottomright.
    if scene_rotation < np.pi / 4:
        ind_order = [0, 1, 2, 3]
    else:
        ind_order = [2, 0, 3, 1]
    calibration_points = np.float32([calibration_points[i] for i in ind_order])

    # adding single axis as is used in openCV
    calibration_points = np.expand_dims(calibration_points, 1)

    return calibration_points

def visualise_scene(scene_image, scene_points):
    if len(scene_points.shape) == 3:
        scene_points = scene_points[:, 0]
    print(scene_points)
    fig, ax = plt.subplots()
    ax.imshow(scene_image, cmap=plt.cm.gray)
    ax.plot(scene_points[:, 0], scene_points[:, 1], 'r+', markersize=15)
    for i, c in enumerate(scene_points):
        # Print found order of the calibration points
        # The calibration points should be ordered in lying position from topleft to bottomright.
        ax.annotate(str(i+1), (c[0] + 5, c[1] - 5), color='r')
    plt.show()


# termination criteria
SUBPIXEL_CRITEREA = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)

def extrinsic_camera_calibration(calibration_image, camera_calibration, scene, verbose=False):
    """
    By estimating image calibration points from a scene and knowing the reference 3D coordinates the extrinsic camera (projection) matrix can be estimated.
    camera_calibration includes the intrinsic calibration matrices
    The estimated rotation matrix of the camera is presented in rodrigues form
    """
    if len(calibration_image.shape) == 3:
        # to grayscale
        calibration_image = cv2.cvtColor(calibration_image, cv2.COLOR_BGR2GRAY)

    chessboard_shape = tuple(scene['shape'])
    # Finding the calibration points in image
    retval, image_points = cv2.findChessboardCorners(calibration_image, chessboard_shape)
    # If found, add object points, image points (after refining them)
    if not retval:
        raise ValueError('Chessboard not found in calibration image')
    cv2.cornerSubPix(calibration_image, image_points, (11,11), (-1,-1), SUBPIXEL_CRITEREA)

    if verbose:
        # Draw and display the corners
        cv2.drawChessboardCorners(calibration_image, chessboard_shape, image_points, retval)
        cv2.imshow('img', calibration_image)
        cv2.waitKey(500)

    # image_points = find_scene(calibration_image)
    # if verbose:
    #     visualise_scene(calibration_image, image_points)
    object_points = calculate_scene_points(scene)

    # Estimate rotation and translation of camera from calibration points
    retval, rvec, tvec = cv2.solvePnP(object_points, image_points, **camera_calibration)
    if not retval:
        raise ValueError('Something wrong happened in estimation of extrinsic camera matrix')

    projection_matrix = { 'rvec': rvec[:, 0], 'tvec': tvec[:, 0] }
    return projection_matrix

def threedimage_plotscale(threedimage):
    """
    Take an image of 3D coordinate values (NxMx3) and scale each dimension to a number between 0 and 1
    This is useful when you want to plot a color image of the 3D reconstruction and get a first look at the results
    nan values can be used for values that are invalid/background
    """
    sum_axes = (0, 1)
    min_v = np.nanmin(threedimage, axis=sum_axes)
    max_v = np.nanmax(threedimage, axis=sum_axes)
    return (threedimage - min_v) / (max_v - min_v)

def estimate_scene_segmentation(scene, calibration_image, projection_matrix, camera_calibration):
    """
    Estimate the scene segmentation.
    Returning the segmentation where 1 is the left scene plane and 2 is the right.
    In addition the 3D scene points are returned.
    """
    if len(calibration_image.shape) == 3:
        # to grayscale
        calibration_image = cv2.cvtColor(calibration_image, cv2.COLOR_BGR2GRAY)
    shape = calibration_image.shape

    scene_points, intersection_point = calculate_scene_points(scene, return_intersection_point=True)
    # estimate 3D coordinates in the center between opposite plane calibration coordinate points
    top_intersection = intersection_point + (scene_points[0] + scene_points[1]) / 2
    bottom_intersection = intersection_point + (scene_points[2] + scene_points[3]) / 2

    scene_points = np.vstack((scene_points, top_intersection, bottom_intersection))[:, 0]
    # project these 3D points into the camera
    image_scene_points, _ = cv2.projectPoints(scene_points, **projection_matrix, **camera_calibration)
    image_scene_points = image_scene_points[:, 0]  # weird thing with projectPoints

    def segmentation_from_polygon(polygon):
        """ Drawing polygon as the segmentation """
        segmentation = np.zeros(shape, dtype=bool)
        rr, cc = draw.polygon(polygon[:, 1], polygon[:, 0], shape)
        segmentation[rr, cc] = True
        return segmentation

    scene_segmentation = np.zeros(shape)
    scene_segmentation[segmentation_from_polygon(image_scene_points[[0, -2, -1, 2]])] = 1
    scene_segmentation[segmentation_from_polygon(image_scene_points[[-2, 1, 3, -1]])] = 2

    # plt.imshow((calibration_image * (scene_segmentation + 1) / 3).astype(int))
    # plt.plot(image_scene_points[:, 0], image_scene_points[:, 1], 'r*')
    # plt.show()
    # exit()
    return scene_segmentation, scene_points
