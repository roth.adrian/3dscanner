#!/usr/bin/python3
import cv2
import os.path as osp
import numpy as np
import matplotlib.pyplot as plt
# import matplotlib.path as mplPath
from skimage import measure, morphology

from fp23dpy import export

from . import helpers

class Plane3D():
    def __init__(self, points=None):
        if not points is None:
            self.estimate(points)

    def estimate(self, points):
        n_points = len(points)
        if points.shape[1] != 3:
            raise ValueError('Only 3D points are supported for this model')
        if n_points < 3:
            raise ValueError('At least three points are required to estimate a plane')
        elif n_points == 3:
            plane_point = points[0]
            plane_normal = np.cross(points[1] - points[0], points[2] - points[0])
        else:
            # Using linear least squares to estimate the plane
            # a + bx + cy = z
            # Au = b
            A = np.hstack((np.ones((n_points, 1)), points[:, :-1]))
            b = points[:, -1:]
            u = np.linalg.lstsq(A, b, None)[0]
            a, b, c = u[:, 0]
            plane_normal = np.array([-b, -c, 1])
            plane_point = np.array([0, 0, a])
        self.point = plane_point
        self.normal = plane_normal / np.linalg.norm(plane_normal)
        return True

    def residuals(self, points):
        """ Calculate residuals of points compared to the estimated plane """
        # Trusting points further to the sides more (large y coordinates)
        weights = np.abs(points[:, 1])
        min_val = 0.7
        weights = (weights / np.mean(weights)) * (1 - min_val) + min_val
        point_vectors = points - self.point
        residuals = np.abs(np.sum(point_vectors * self.normal, axis=-1)) / weights
        # plt.hist(residuals, 30)
        # plt.show()
        # exit()
        return residuals

    def project_lines(self, line_points, line_directions):
        """ Project 3D lines onto the 3D plane """
        line_lengths = np.sum(self.normal * (self.point - line_points), axis=-1) / np.sum(self.normal * line_directions, axis=-1)
        plane_projections = line_points + np.expand_dims(line_lengths, 1) * line_directions
        return plane_projections

    def __str__(self):
        return 'Plane3D\tpoint:  {}\n\tnormal: {}'.format(self.point, self.normal)


def project_pixels_to_plane(pixels, plane, rvec, tvec, cameraMatrix, distCoeffs):
    """
    pixels to project onto plane that is a Plane3D object.
    The rvec and tvec are camera projection matrix and cameraMatrix and distCoeffs the intrinsic parameters for the camera
    """
    ###### Intrinsic parameter fixing of the points ####
    # Fixing weird opencv dimensions thing and type of pixels array
    pixels = np.expand_dims(pixels, 1).astype(np.float32)
    # removing pixel stuff on image plane
    image_plane_points = cv2.undistortPoints(pixels, cameraMatrix, distCoeffs)
    # removing weird single dimension in return array
    image_plane_points = image_plane_points[:, 0, :]
    # making points homogeneous
    image_plane_points = np.hstack((image_plane_points, np.ones((pixels.shape[0], 1))))

    # Calculating 3D rotation matrix and camera center for camera
    R = cv2.Rodrigues(rvec)[0]
    camera_center = -np.sum(R.T * tvec, axis=1)

    # Estimating the ray projections
    ray_directions = (R.T @ image_plane_points.T).T
    plane_projections = plane.project_lines(camera_center, ray_directions)
    return plane_projections


def threedscan(directory, laser=False, verbose=False):
    images = helpers.listdir_images(directory)
    if len(images) <= 1:
        # Trying to find video in directory to split into frames
        videos = helpers.listdir(directory, ['.avi', '.mp4'])
        if len(videos) == 0:
            raise ValueError('No files with valid image or video format found.')
        # assuming only single video
        helpers.split_clip(videos[0])
        images = helpers.listdir_images(directory)

    # Image used for extrinsic calibration and to get the image shape
    calibration_image = cv2.imread(images[0], cv2.IMREAD_GRAYSCALE)
    shape = calibration_image.shape

    # Config contains information of where camera calibration is and the scene geometry
    config_path = osp.join(directory, 'config.json')
    config = helpers.read_json(config_path)

    camera_path = osp.join(directory, config['camera'])
    camera_calibration = helpers.read_json(camera_path)

    ####### Calibrate extrinsic camera parameters #######
    # projection matrix is dict including rvec and tvec
    if 'projection_matrix' in config:
        projection_matrix = config['projection_matrix']
    else:
        projection_matrix = helpers.extrinsic_camera_calibration(calibration_image, camera_calibration, config['scene'])
        config['projection_matrix'] = projection_matrix
        helpers.write_json(config_path, config)
    if 'real_projection_matrix' in config and verbose:
        print('Simulated swept-planes extrinsic estimation check')
        print('Correct:')
        print(config['real_projection_matrix'])
        print('Estimated:')
        print(projection_matrix)
    # if 'real_projection_matrix' in config:
    #     projection_matrix = config['real_projection_matrix']

    # calculate coordinates for all pixels in the image
    pixel_coordinates_flat = np.array(np.meshgrid(np.arange(shape[1]), np.arange(shape[0]))).reshape(2, -1).T
    pixel_coordinates = pixel_coordinates_flat.reshape(shape + (-1,))

    ######## Estimate scene segmentation #######
    scene_segmentation, scene_points = (
                    helpers.estimate_scene_segmentation(config['scene'], calibration_image,
                                                        projection_matrix, camera_calibration))

    ######## Estimate the 3D plane coordinate for each scene pixel #######
    left_plane = Plane3D(scene_points[[0, 2, -2]])  # three 3D points define a plane
    right_plane = Plane3D(scene_points[[1, -2, 3]])

    scene_pixels_on_plane = np.full(shape + (3,), np.nan)
    # estimating left calibration plane
    scene_pixels_on_plane[scene_segmentation == 1] = project_pixels_to_plane(pixel_coordinates[scene_segmentation == 1], left_plane,
                                                                       **projection_matrix, **camera_calibration)
    # estimating right calibration plane
    scene_pixels_on_plane[scene_segmentation == 2] = project_pixels_to_plane(pixel_coordinates[scene_segmentation == 2], right_plane,
                                                                       **projection_matrix, **camera_calibration)
    # plt.imshow(helpers.threedimage_plotscale(scene_pixels_on_plane))
    # plt.show()
    # exit()


    ######## Find shadow planes per frame ##########
    n_images = len(images)
    # find mid pixel value for each pixel in the time series
    frames = np.zeros((n_images,) + shape)
    if laser:
        # red laser is used as plane, try to enhance the red color
        def read_frame(path):
            frame = cv2.imread(path)
            # opencv has BGR channels
            # if returned pixel value is low there is much red in the pixel
            return 255 - (frame[:, :, 2] - np.mean(frame[:, :, :2], axis=2))
    else:
        # shadows as the plane
        def read_frame(path):
            return cv2.imread(path, cv2.IMREAD_GRAYSCALE)

    for i, f in enumerate(images):
        # frames[i] = cv2.imread(images[i], cv2.IMREAD_GRAYSCALE)
        frames[i] = read_frame(f)  # cv2.imread(images[i], cv2.IMREAD_GRAYSCALE)

    mid_frame_values = (np.min(frames, axis=0) + np.max(frames, axis=0)) / 2
    diffed_frames = frames - mid_frame_values
    # find where the crossing of the mid value occures, only for the front 
    crossing = np.argmax(diffed_frames < 0 , axis=0)
    crossing[~scene_segmentation.astype(bool)] = 0
    # plt.imshow(crossing)
    # plt.show()
    # exit()

    # estimate two lines in image, one for each calibration plane using ransac
    # x values to use in finding plane points
    foreground_segmentation = np.zeros(shape, dtype=bool)
    ransac_params = { 'model_class': Plane3D, 'min_samples': 3, 'residual_threshold': 10 }
    planes = np.zeros(n_images, dtype=object)
    for i in range(1, n_images):
        this_frame = (crossing == i) & (scene_segmentation != 0)
        if np.sum(this_frame) < 10:
            continue
        coordinates3D = scene_pixels_on_plane[this_frame]
        plane, inliers = measure.ransac(coordinates3D, **ransac_params)
        this_frame[this_frame] = ~inliers
        foreground_segmentation[this_frame] = True
        planes[i] = plane

    ######## Triangulate 3D coordinates ##########
    # find foreground of scene
    # morphology
    foreground_segmentation = morphology.opening(foreground_segmentation, morphology.disk(5))
    # foreground_segmentation = morphology.erosion(foreground_segmentation, morphology.disk(5))
    if np.sum(foreground_segmentation) < 20:
        raise ValueError("Foreground segmentation has to few pixels")
    # plt.imshow(foreground_segmentation)
    # plt.show()
    # exit()
    # foreground_segmentation = scene_segmentation.astype(bool) & (crossing != 0)

    # The shadow frames for the foreground to reconstruct
    foreground_frame_inds = np.zeros(shape, dtype=int)
    foreground_frame_inds[foreground_segmentation] = crossing[foreground_segmentation]
    # checking which frames and removing the zero value
    frames_to_check = np.unique(foreground_frame_inds)[1:]
    threed_coordinates = np.full(shape + (3,), np.nan)
    for i in frames_to_check:
        this_frame = foreground_frame_inds == i
        # triangulate out the 3D coordinated using the lines from each pixel and the plane corresponding to the pixels
        threed_coordinates[this_frame] = project_pixels_to_plane(pixel_coordinates[this_frame], planes[i],
                                                                 **projection_matrix, **camera_calibration)
    if verbose:
        plt.imshow(helpers.threedimage_plotscale(threed_coordinates))
        plt.show()

    out_threed_coordinates = np.transpose(threed_coordinates, axes=(2, 0, 1)) / 1000  # to meter
    # rotation for fp23dpy export to 3D
    out_threed_coordinates = np.stack((out_threed_coordinates[1],
                                       out_threed_coordinates[2],
                                       -out_threed_coordinates[0]))
    # plt.imshow(out_threed_coordinates[0])
    # plt.show()

    output_filename = osp.join(directory, 'reconstruction.glb')
    export.export3D(output_filename, { 'grid': out_threed_coordinates })
    # celebrate!!
