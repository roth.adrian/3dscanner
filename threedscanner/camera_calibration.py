#!/usr/bin/python3
"""
Calibrate camera after recording images of chessboard config grid with different orientations.
"""
import cv2
import os.path as osp
import numpy as np

from . import helpers
from .helpers import extrinsic_camera_calibration
extrinsic_camera_calibration

def basic_calibration_target():
    path = osp.join(osp.dirname(__file__), '../grids/chessboard_config.json')
    return helpers.read_json(path)

def calibrate_camera(calibration_location, extrinsic=False, verbose=False):
    """
    Calibrate camera after recording images of chessboard grid or other calibration type with different orientations.
    calibration_location is the directory of file with the calibration images images
    chessboard_config is the file that contains information os grid shape and square size in grid (in mm or whatever unit)

    if extrinsic do extrinsic camera calibration by assuming that calibration image file is a scene with four calibration points in the background and show results
    """

    if extrinsic:
        # Only a single image is allowed for this 
        calibration_file = calibration_location
        if not osp.isfile(calibration_file):
            raise ValueError('Extrinsic calibration only accepts a single calibration image file')

        directory = osp.dirname(calibration_file)
        config_path = osp.join(directory, 'config.json')
        if not osp.isfile(config_path):
            raise ValueError('Config file with scene not found')
        
        config = helpers.read_json(config_path)

        camera_key = osp.basename(calibration_file).split('_')[0]
        if not 'camera' in camera_key:
            camera_key = 'camera'
        camera_path = osp.join(directory, config[camera_key])
        camera_calibration = helpers.read_json(camera_path)

        calibration_image = cv2.imread(calibration_file, cv2.IMREAD_GRAYSCALE)

        projection_matrix = helpers.extrinsic_camera_calibration(calibration_image, camera_calibration, config['scene'], verbose=verbose)
        config['projection_matrix'] = projection_matrix
        helpers.write_json(config_path, config)
        return

    # Reading size and shapes of the squares
    config_path = osp.join(calibration_location, 'config.json')
    config_grid = helpers.read_json(config_path)
    square_width = config_grid['square_width']
    shape = tuple(config_grid['shape'])

    # prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
    objp = np.zeros((shape[0] * shape[1], 3), np.float32)
    objp[:, :2] = np.mgrid[0:shape[0], 0:shape[1]].T.reshape(-1, 2) * square_width

    # Arrays to store object points and image points from all the images.
    objpoints = [] # 3d point in real world space
    imgpoints = [] # 2d points in image plane.

    images = helpers.listdir(calibration_location, ['.png', '.tif'])

    for fname in images:
        img = cv2.imread(fname, cv2.IMREAD_GRAYSCALE)

        # Find the chess board corners
        ret, corners = cv2.findChessboardCorners(img, shape, None)

        # If found, add object points, image points (after refining them)
        if ret:
            objpoints.append(objp)

            cv2.cornerSubPix(img, corners, (11,11), (-1,-1), helpers.SUBPIXEL_CRITEREA)
            imgpoints.append(corners)

            # Draw and display the corners
            cv2.drawChessboardCorners(img, shape, corners, ret)
            cv2.imshow('img', img)
            cv2.waitKey(500)
        else:
            print('Warning: {} not used.'.format(fname))

    reprojection_error, K, dc, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, img.shape, None, None)
    if verbose:
        print('Reprojection error', reprojection_error)
        # Testing the result
        img = cv2.imread(images[0])
        img_shape = img.shape[:2][::-1]
        newcameramtx, roi = cv2.getOptimalNewCameraMatrix(K, dc, img_shape, 1, img_shape)
        # undistort
        dst = cv2.undistort(img, K, dc, None, newcameramtx)
        # crop the image
        x,y,width,height = roi
        dst = dst[y:y+height, x:x+width]
        cv2.imshow('img', img)
        cv2.imshow('undisorted', dst)
        cv2.waitKey(0)

    if 'simulated' in calibration_location:
        # This is a simulated camera where the correct result is known
        correct_config_file = osp.join(calibration_location, 'calibration.json')
        calibration = helpers.read_json(correct_config_file)
        print('Simulated calibration directory estimation check')
        print('Correct:')
        print(np.array(calibration['cameraMatrix']))
        print(np.array(calibration['distCoeffs']))
        print('Estimated:')
        print(K)
        print(dc)
        # i_error = np.linalg.norm(K - calibration['intrinsic_camera_matrix'])
        # d_error = np.linalg.norm(dc - calibration['distortion_coefficients'])

        # Using the correct camera calibration instead of the estimated one
        K = np.mat(calibration['cameraMatrix'])
        dc = np.mat(calibration['distCoeffs'])
        reprojection_error = 0

    calibration = { 'cameraMatrix': K,
                    'distCoeffs': dc }
    outfile = osp.join(calibration_location, 'calibration.json')
    helpers.write_json(outfile, calibration)
